package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@ModelAttribute(name="start") String start,
			@ModelAttribute(name="end") String end) throws Exception {
		ModelAndView mav = new ModelAndView();
		// 投稿を取得
		List<Report> contentData = reportService.findAllReport(start, end);
		//コメントを全件取得
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		mav.addObject("start", start);
		mav.addObject("end", end);

		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") int id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	//編集処理
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Report report = reportService.editReport(id);
		// 編集する投稿をセット
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント投稿画面
	@GetMapping("/newComment/{id}")
	public ModelAndView newComment(@PathVariable("id") int id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Comment comment = new Comment();
		comment.setReportId(id);
		// 準備した空のentity(comment)を保管
		mav.addObject("commentModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/newComment");
		return mav;
	}

	//削除処理
	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable("id") int id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

	//コメント投稿処理
	@PostMapping("/addComment/{id}")
	public ModelAndView addComment(@PathVariable("id") int id, @ModelAttribute("commentModel") Comment comment) {
		// コメント投稿をテーブルに格納
		comment.setReportId(id);
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント編集処理
	@GetMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable("id") int id) {
		ModelAndView mav = new ModelAndView();
		// 編集するコメントを取得
		Comment comment = commentService.editComment(id);
		// 編集するコメントをセット
		mav.addObject("commentModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/editComment");
		return mav;
	}

	//コメント編集処理
	@PutMapping("/updateComment/{id}")
	public ModelAndView updateComment(@PathVariable int id, @ModelAttribute("commentModel") Comment comment,
			int reportId) {
		// UrlParameterのidを更新するentityにセット
		comment.setId(id);
		comment.setReportId(reportId);
		// 編集した投稿を更新
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

}
