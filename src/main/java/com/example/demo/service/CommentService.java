package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	// コメントレコード全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAllByOrderByCreatedDesc();
	}

	//コメントレコード追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	//コメントレコード一件取得
	public Comment editComment(int id) {
		Comment comment = (Comment)commentRepository.findById(id).orElse(null);
		return comment;
	}

	//コメントレコード削除
	public void deleteComment(int id) {
		commentRepository.deleteById(id);
	}
}