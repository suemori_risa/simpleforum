package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	//public List<Report> findAllReport() {
	//return reportRepository.findAll();
	//}

	public List<Report> findAllReport(String start, String end) throws ParseException {

		if (!StringUtils.isEmpty(start)) {
			start = start + " 00:00:00";
		} else {
			start = "2020-01-01 00:00:00";
		}
		if (!StringUtils.isEmpty(end)) {
			end = end + " 23:59:59";
		} else {
			Date date = new Date();
			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			end = sdFormat.format(date);
		}

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date earlyStart = sdFormat.parse(start);
		Date lastEnd = sdFormat.parse(end);

		return reportRepository.findByCreatedBetweenOrderByCreatedDesc(earlyStart, lastEnd);
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	//レコード削除
	public void deleteReport(int id) {
		reportRepository.deleteById(id);
	}

	// レコード1件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}
}
